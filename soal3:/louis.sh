#!/bin/bash

echo "REGISTER"
echo "USERNAME: "
read username
echo "PASSWORD: "
read pass


if [[ "$pass" =~ ^[[:alnum:]]+$ ]] &&
	[[ "$pass" =~ [[:upper:]] ]] &&
	[[ "$pass" =~ [[:lower:]] ]] &&
	[[ "$pass" != *"chicken"* ]] &&
	[[ "$pass" != *"ernie"* ]] &&
	[[ "$pass" != "$username" ]] &&
	[[ ${#pass} -ge 8 ]]
		then password="$pass"

	timestamp=$(date +"%y/%m/%d %H:%M:%S")

	checkusername="$(grep -o "$username" users/users.txt)"
	if [ "$checkusername" ]
		then message="REGISTER: ERROR $username already exists"
	else
		echo "$username $password" >> users/users.txt
		message="REGISTER: INFO $username registered successfully"
	fi

	echo "$message"
	echo "$timestamp $message " >> log.txt

else

	echo "Your password does not match the requirement"
fi
