soal 1

nama file csv saya rename menjadi x.csv untuk mempermudah pengerjaan <br />

1. grep lines yang memiliki kata "Japan" pada file x.csv kemudian hasilnya diambil 5 teratas menggunakan head -n 5 <br />
2. hasil dari 1a. di sort berdasarkan kolom ke 9 (fsr) dalam ascending order, -n untuk sort dalam numerical stardard, -t ',' untuk mengisyaratkan bahwa data dipisahkan oleh tanda ',' <br />
3. grep lines yang memiliki kata "Japan" pada file x.csv kemudian hasilnya di sort berdasarkan kolom ke 20 (ger) dalam ascending  order, -n untuk sort dalam numerical stardard, -t ',' untuk mengisyaratkan bahwa data dipisahkan oleh tanda ',' kemudian diambil 10 teratas dengan head -n 10 <br />
4. grep lines yang memiliki kata "keren" pada file x.csv <br />
![](Dokumentasi/No_1.jpg)

Kesulitan : tidak ada <br />

soal 2

Algoritma :<br />
      1. Path setting: Skrip menetapkan jalur ke file skrip. <br />
      2. Cron job / sleep: Menunda atau menjeda. <br />
      3. Pembuatan direktori: Skrip menetapkan nama direktori unduhan dan membuatnya jika tidak ada menggunakan perintah mkdir. <br />
      4. Conditional statements: Skrip menggunakan pernyataan if untuk menetapkan nilai variabel berdasarkan waktu akses. <br />
      5. Looping: Skrip menggunakan for looping untuk membaca URL dari file dan mengunduh gambar. <br />
      6. Renaming: Skrip me-rename nama file dengan urutan nomor menggunakan interpolasi string. <br />
      7. Pengunduhan file: Skrip mengunduh gambar menggunakan perintah wget dan menyimpannya dengan nama file yang dibuat. <br />
      8. Zip: Skrip menggunakan perintah zip untuk mengompres folder menjadi file ZIP. <br />
![](Dokumentasi/kobeni_liburan.jpeg)



Catatan tambahan :
	Membuat Folder, zip, dan crontab <br />
Kesulitan :
Membuat Crontab <br />



soal 3

louis.sh (sistem register)
![](Dokumentasi/nano_reg.jpg)

![](Dokumentasi/register.jpg)


retep.sh sebagai sistem login
![](Dokumentasi/nano_login.jpg)

![](Dokumentasi/log_in.jpg)

Log.txt
![](Dokumentasi/log.txt.jpg)

Kesulitan : sempat terkendala membuat log.txt

soal 4

Penjelasan log_encrypt
![](Dokumentasi/encrypt_explanation.jpg)

Penjelasan log_decrypt
![](Dokumentasi/decrypt_explanation.jpg)

Run code
![](Dokumentasi/No_4.jpg)

Encrypted
![](Dokumentasi/encrypted.jpg)

Decrypted
![](Dokumentasi/decrypted.jpg)

