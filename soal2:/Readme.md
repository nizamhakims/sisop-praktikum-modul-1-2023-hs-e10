Algoritma :
      1. Path setting: Skrip menetapkan jalur ke file skrip.
      2. Cron job / sleep: Menunda atau menjeda.
      3. Pembuatan direktori: Skrip menetapkan nama direktori unduhan dan membuatnya jika tidak ada menggunakan perintah mkdir.
      4. Conditional statements: Skrip menggunakan pernyataan if untuk menetapkan nilai variabel berdasarkan waktu akses.
      5. Looping: Skrip menggunakan for looping untuk membaca URL dari file dan mengunduh gambar.
      6. Renaming: Skrip me-rename nama file dengan urutan nomor menggunakan interpolasi string.
      7. Pengunduhan file: Skrip mengunduh gambar menggunakan perintah wget dan menyimpannya dengan nama file yang dibuat.
      8. Zip: Skrip menggunakan perintah zip untuk mengompres folder menjadi file ZIP.
