#!/bin/bash

# Set the path to the script
script_path="/home/eileithyial/kobeni_liburan_1.sh"

# Construct the cron job command to run the script every 10 hours
cron_job="0 */10 * * * ${script_path}"

# Write the cron job to a temporary file
echo "${cron_job}" > cron.tmp

# Add the cron job to the crontab
crontab cron.tmp

# Remove the temporary file
rm cron.tmp

# Set the download directory and create it if it doesn't exist
folder_name="kumpulan"
mkdir -p "$folder_name"

# Set the start time and end time for downloading
start_time=$(date +%H)

# Sets the value of the counter variable based on the value of the start_time variable
if [ "$start_time" == "00" ]; then
  counter=1
else
  counter="$start_time"
fi

# Read the URLs from the file and download the images
for ((i=0; i< $counter; i++)); do
    # Construct the file name with a number sequence
    file_name="${folder_name}/perjalanan_$((i+1)).file"
    # Download the image and save it with the constructed file name
    wget -o "$file_name" "https://img4.utiket.com/images/uploads/tpimages/pantai-mawi.jpg"
    # If you want to use sleep for delay instead cron 
    # sleep 10h
done

# Zip the folder
zip -r "Devil.zip" "$folder_name"

# this crontab -e : (0 */10 * * * /home/eileithyial/kobeni_liburan.sh


