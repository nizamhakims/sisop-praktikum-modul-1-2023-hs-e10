#!/bin/bash

jam=$(date +"%k")

huruf="abcdefghijklmnopqrstuvwxyz"
huruf=$huruf$huruf
hurufKapital=$(echo $huruf | tr [a-z] [A-Z])

nama_file=$(date +"%H:%M %d:%m:%Y")

cat /var/log/syslog | tr [${huruf:26}${hurufKapital:26}] [${huruf:$jam:26}${hurufKapital:$jam:26}] > /home/nizam/Downloads/encryptedLog/"$nama_file"

# Crontab
# 0 */2 * * * /bin/bash /home/nizam/Donwloads/log_encrypt.sh