#!/bin/bash

huruf="abcdefghijklmnopqrstuvwxyz"
huruf=$huruf$huruf
hurufKapital=$(echo $huruf | tr [a-z] [A-Z])

for f in /home/nizam/Downloads/encryptedLog/*; do
        if [[ $(echo $f | grep "decrypted") && $? -eq 0 ]]; then
                continue
        fi

        file=$(basename "$f")
        jam=${file:0:2}
        nama_file="$file decrypted"

        cat "$f" | tr [${huruf:$jam:26}${hurufKapital:$jam:26}] [${huruf:0:26}${hurufKapital:0:26}] > /home/nizam/Downloads/decryptedLog/"$nama_file"
done